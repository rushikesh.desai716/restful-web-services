package com.in28minutes.rest.webservice.restfulwebservices.user;

import com.in28minutes.rest.webservice.restfulwebservices.jpa.PostRepository;
import com.in28minutes.rest.webservice.restfulwebservices.jpa.UserRepository;
import jakarta.validation.Valid;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class UserJpaResource {

    private final UserRepository userRepository;

    private final PostRepository postRepository;

    public UserJpaResource(UserRepository userRepository, PostRepository postRepository) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }

    @GetMapping("/jpa/users")
    public List<User> retrieveAllUsers() {
        return userRepository.findAll();
    }

    /*Important concepts in HATEOAS:
        -	EntityModel
		-	WebMvcLinkBuilder*/
    @GetMapping("/jpa/users/{id}")
    public EntityModel<User> retrieveUser(@PathVariable int id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty())
            throw new UserNotFoundException("id:" + id);

        EntityModel<User> entityModel = EntityModel.of(user.get());

        //  Specify the MVC Controller method. DO NOT hard-code the URL because URL may change in future.
        WebMvcLinkBuilder linkAllUsers = linkTo(methodOn(this.getClass()).retrieveAllUsers());
        WebMvcLinkBuilder linkUser = linkTo(methodOn(getClass()).retrieveUser(id));

        //  Add links in the entityModel
        entityModel.add(linkAllUsers.withRel("all-users"));
        entityModel.add(linkUser.withRel("user-" + id));

        return entityModel;
    }

    @PostMapping("/jpa/users")
    public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
        User savedUser = userRepository.save(user);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedUser.getId())
                .toUri();

        return ResponseEntity.created(location).build();    //  Sending Response code 201:CREATED
    }

    @DeleteMapping("/jpa/users/{id}")
    public void deleteUser(@PathVariable int id) {
        userRepository.deleteById(id);
    }

    @GetMapping("/jpa/users/{id}/posts")
    public List<Post> retrievePostsForUser(@PathVariable int id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty())
            throw new UserNotFoundException("id:" + id);

        return user.get().getPosts();
    }

    @PostMapping("/jpa/users/{id}/posts")
    public ResponseEntity<Object> createPostForUser(@PathVariable int id, @Valid @RequestBody Post post) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty())
            throw new UserNotFoundException("id:" + id);

        post.setUser(user.get());

        Post savedPost = postRepository.save(post);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedPost.getId())
                .toUri();

        return ResponseEntity.created(location).build();    //  Sending Response code 201:CREATED
    }

    @GetMapping("/jpa/users/{id}/posts/{postId}")
    public Post retrieveOnePost(@PathVariable int id, @PathVariable int postId) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty())
            throw new UserNotFoundException("id:" + id);

        Optional<Post> post = postRepository.findById(postId);
        if (post.isEmpty())
            throw new PostNotFoundException("postId:" + postId);

        return user.get().getPosts().stream()
                .filter(element -> element.getId().equals(post.get().getId()))
                .findFirst().orElse(null);
    }

    @DeleteMapping("/jpa/users/{id}/posts/{postId}")
    public void deletePostOfUser(@PathVariable int id, @PathVariable int postId) {
        postRepository.deleteById(postId);
    }
}
