package com.in28minutes.rest.webservice.restfulwebservices.filtering;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FilteringController {

//    @GetMapping("/filtering")
//    public SomeBean filtering() {
//        return new SomeBean("value1", "value2", "value3", "field4");
//    }

    @GetMapping("/filtering")
    public MappingJacksonValue filtering() {

        SomeBean someBean = new SomeBean("value1", "value2", "value3", "value4");

        MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(someBean);

        applyDynamicFilter(mappingJacksonValue, "field1", "field3");    // Providing different fields for Dynamic Filtering

        return mappingJacksonValue;
    }

//    @GetMapping("/filtering-list")
//    public List<SomeBean> filteringList() {
//        return List.of(new SomeBean("value1", "value2", "value3", "field4"),
//                new SomeBean("value11", "value22", "value33", "field44"));
//    }

    @GetMapping("/filtering-list") //field2, field3
    public MappingJacksonValue filteringList() {
        List<SomeBean> list = List.of(new SomeBean("value1", "value2", "value3", "value4"),
                new SomeBean("value11", "value22", "value33", "value44"));

        MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(list);

        applyDynamicFilter(mappingJacksonValue, "field2", "field3");    // Providing different fields for Dynamic Filtering

        return mappingJacksonValue;
    }

    //  Helper method to reduce code duplication.
    private static void applyDynamicFilter(MappingJacksonValue mappingJacksonValue, String... fields) {
        SimpleBeanPropertyFilter filter =
                SimpleBeanPropertyFilter.filterOutAllExcept(fields);

        FilterProvider filters =
                new SimpleFilterProvider().addFilter("SomeBeanFilter", filter);

        mappingJacksonValue.setFilters(filters);
    }
}
