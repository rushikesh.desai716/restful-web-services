package com.in28minutes.rest.webservice.restfulwebservices.versioning;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersioningPersonController {

    //  URI Versioning
    @GetMapping("/v1/person")                       //  http://localhost:8080/v1/person
    public PersonV1 getFirstVersionOfPerson() {
        return new PersonV1("Bob Charlie");
    }

    @GetMapping("/v2/person")                       //  http://localhost:8080/v2/person
    public PersonV2 getSecondVersionOfPerson() {
        return new PersonV2(new Name("Bob", "Charlie"));
    }

    //  Request Parameter Versioning
    @GetMapping(path = "/person", params = "v=1")       //  http://localhost:8080/person?v=1
    public PersonV1 getFirstVersionOfPersonRequestParam() {
        return new PersonV1("Joey Tribbiani");
    }

    @GetMapping(value = "/person", params = "v=2")      //  http://localhost:8080/person?v=2
    public PersonV2 getSecondVersionOfPersonRequestParam() {
        return new PersonV2(new Name("Joey", "Tribbiani"));
    }

    //  Request Headers Versioning
    @GetMapping(path = "/person/header", headers = "X-API-V=1")       //  http://localhost:8080/person/header with Header = X-API-V, Value = 1
    public PersonV1 getFirstVersionOfPersonRequestHeaders() {
        return new PersonV1("Ross Geller");
    }

    @GetMapping(value = "/person/header", headers = "X-API-V=2")      //  http://localhost:8080/person/header with Header = X-API-V, Value = 2
    public PersonV2 getSecondVersionOfPersonRequestHeaders() {
        return new PersonV2(new Name("Ross", "Geller"));
    }

    @GetMapping(path = "/person/accept", produces = "application/vnd.company.app-v1+json")
    public PersonV1 getFirstVersionOfPersonAcceptHeader() {
        return new PersonV1("Bob Charlie");
    }

    //  Media Type Versioning
    @GetMapping(path = "/person/accept", produces = "application/vnd.company.app-v2+json")
    public PersonV2 getSecondVersionOfPersonAcceptHeader() {
        return new PersonV2(new Name("Bob", "Charlie"));
    }
}
